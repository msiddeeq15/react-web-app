import * as React from 'react';
import NavBar, { TabProps } from './NavBar';
import './AppNavigator.less';

export interface ContentWrapperProps { 
  children: any;
  tabs: Array<TabProps>
}

const AppNavigator = (props: ContentWrapperProps) => (
 <div className="AppNavigator">
    <div className="AppNavigator-content">
      { props.children }
    </div>
    <NavBar className="AppNavigator-navBar" tabs={ props.tabs }/>
 </div>
);

export default AppNavigator;