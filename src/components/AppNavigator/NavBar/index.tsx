import * as React from 'react';
import { Link } from "react-router-dom";
import classnames from 'classnames';
import { Icon } from '@Components';
import './NavBar.less';

export interface TabProps {
  href: string;
  icon: string;
  name: string;
}

export interface NavBarProps {
  className: string; 
  tabs: Array<TabProps>; 
}

const NavBar = (props: NavBarProps) => (
 <div className={ classnames('NavBar', props.className ) }>
    {
      props.tabs.map(({ href, name, icon }, i) => (
        <Link
          key={ `${ i }-${ icon }` }
          to={ href }
          className={ classnames('NavBar-tab', { active: window.location.pathname === href }) }
        >
          <Icon kind={ icon } />
          <span className="label">{ name }</span>
        </Link>
      ))
    }
 </div>
);

export default NavBar;