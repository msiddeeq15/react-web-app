import * as React from "react";
import classnames from 'classnames';
import { Icon } from '@Components';

export interface SidebarProps {
  className: string;
  active: boolean;
  children: any;
  handleClose?: () => void
}

const Sidebar = (props: SidebarProps) => (
  <div className={ classnames('Sidebar', props.className, { active: props.active }) }>
    <Icon className="close-button" kind="close" onClick={ () => props.handleClose() } />
    { props.children }
  </div>
);

export default Sidebar;