import * as React from 'react';
import classname from 'classnames';
import { LoadingContainer } from '@Components';
import Sidebar from './Sidebar';
import './Page.less';

export interface PageProps {
  page: string;
  title?: string;
  pageLoading?: boolean;
  pageError?: boolean;
  pageErrorDisplay?: any;
  leftContent?: any;
  rightContent?: any;
  children: any;
  sidebar?: any;
  sidebarActive?: boolean;
  handleCloseSidebar?: () => void;
}

const Page = (props: PageProps) => (
  <div className={ classname('Page', `${ props.page }-Page`) }>
    <div className="Page-header">
      <div className="Page-header-leftContent">{ props.leftContent }</div>
      <h1 className="Page-title">
        { props.title }
      </h1>
      <div className="Page-header-rightContent">{ props.rightContent }</div>
    </div>
    <div className="Page-content">
      <LoadingContainer
        className="Page-loadingContainer"
        loading={ Boolean(props.pageLoading) }
        error={ Boolean(props.pageError) }
        errorDisplay={ props.pageErrorDisplay }
      >
        <div className={ classname('Page-content-main', { sidebarActive: props.sidebar && props.sidebarActive })}>
          { props.children }
        </div>
        {
          props.sidebar ? (
            <Sidebar className="Page-content-sidebar" active={ props.sidebarActive } handleClose={ props.handleCloseSidebar }>
              { props.sidebar }
            </Sidebar>
          ) : ''
        }
      </LoadingContainer>
    </div>
  </div>
);

export default Page;