import * as React from 'react';
import classnames from 'classnames';
import './LoadingContainer.less';

export interface LoadingConatinerProps {
  className?: string;
  loading: boolean;
  error?: boolean;
  errorDisplay?: any,
  children: any
}

const LoadingContainer = (props: LoadingConatinerProps) => {
  if(props.loading) {
    return (
      <div className={ classnames('LoadingContainer loading', props.className) } />
    );
  } else if (props.error) {
    return (
      <div className={ classnames('LoadingContainer error', props.className) }>
        { props.errorDisplay || 'There was an error' }
      </div>
    );
  } else {
    return (
      <React.Fragment>
        { props.children }
      </React.Fragment>
    )
  }
}

export default LoadingContainer;
