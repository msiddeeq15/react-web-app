import * as React from 'react';
import classnames from 'classnames';
import './List.less';

export interface ListProps {
  className?: string,
  listData: Array<any>,
  listItemComponent: any,
  onItemClick?: (e: Object, listItem: { index: number }) => void
}

const List = (props: ListProps) => {
  const listProps = {
    className: classnames(
      'List',
      props.className,
      { empty: props.listData.length === 0 },
      { activeItems: Boolean(props.listData.find((listItem) => listItem.active)) }
    )
  };

  const commonListItemProps = {
    className: 'List-item',
    onClick: (e: Object, listItem: { index: number }) => props.onItemClick && props.onItemClick(e, listItem)
  };

  return (
    <div { ...listProps }>
      {
        props.listData.map((listItemProps, index) => {
          const itemProps: any = {
            ...commonListItemProps,
            ...listItemProps,
            className: classnames(commonListItemProps.className, listItemProps.className, { active: listItemProps.active }),
            key: index,
            index
          };

          return <props.listItemComponent { ...itemProps } />;
        })
      }
    </div>
  );
};

export default List;