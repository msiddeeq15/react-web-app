import * as React from 'react';
import classnames from 'classnames';
import './Icon.less';

export interface IconProps { 
  kind: string;
  size?: 'sm' | 'sm';
  href?: string;
  onClick?: (e: Object) => void;
  className?: string;
}

const Icon = (props: IconProps) => {
  const clickable = Boolean(props.href || props.onClick);
  const iconProps = {
    className: classnames('Icon', `Icon-size-${ props.size }`, { 'Icon-button': clickable }, props.className),
    ...(clickable ? { onClick: props.onClick } : {})
  };

  return (
    <div { ...iconProps }>
       <img src={ require(`@Assets/images/${ props.kind}@2x.png`) } height="100%"/>
    </div>
   );
};

Icon.defaultProps = {
  size: 'sm'
};

export default Icon;