function getMidPoint(points: Array<[number, number]>): [number, number] {
  const [ rangeX, rangeY ] = points.reduce((
    [
      [ minX, maxX ],
      [ minY, maxY ]
    ],
    [x, y]
  ) => {
    return [ [ Math.min(minX, x), Math.max(maxX, x) ], [ Math.min(minY, y), Math.max(maxY, y) ] ];
  }, [[Infinity, -Infinity],[Infinity, -Infinity]]);
  const [ distanceX, distanceY ] = [ Math.abs(rangeX[0] - rangeX[1]), Math.abs(rangeY[0] - rangeY[1])  ];

  return [ rangeX[0] + distanceX / 2.0, rangeY[0] + distanceY / 2.0 ];
}

export default getMidPoint;