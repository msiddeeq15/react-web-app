import * as React from "react";
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux';
import { withRestaurants } from './store/dataHOCs';
import store from './store/reducers';
import { AppNavigator } from '@Components';
import { Internet, Lunch } from '@Pages';
import './common.less';

const navBarTabs = [
  { 
    name: 'lunch',
    icon: 'lunch',
    href: '/'
  },
  { 
    name: 'internets',
    icon: 'pointer',
    href: '/internet/'
  }
];

const AppRouter = () => (
  <Provider store={ store }>
    <Router>
      <AppNavigator tabs={ navBarTabs }>
        <Route path="/" exact component={ withRestaurants(Lunch) } />
        <Route path="/internet/" component={ Internet } />
      </AppNavigator>
    </Router>
  </Provider>
);

ReactDOM.render(<AppRouter />, document.querySelector('#App'));
