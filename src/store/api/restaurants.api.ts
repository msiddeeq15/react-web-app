import axios from 'axios';

export const getRestautrantsAPI = () => {
  /* won't work on https */
  // const url = 'http://sandbox.bottlerocketapps.com/BR_iOS_CodingExam_2015_Server/restaurants.json';
  
  const url = '/api/restaurants';
  const mapToPayload = (response: any): any => ({ restaurants: response.data.restaurants });

  return axios.get(url).then(mapToPayload);
};
