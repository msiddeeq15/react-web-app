import { getRestautrantsAPI } from '../api/restaurants.api';

export const actionTypes = {
  REQUEST: 'restaurants/GET/REQUEST',
  SUCCESS: 'restaurants/GET/SUCCESS',
  ERROR: 'restaurants/GET/ERROR'
};

export const getRestaurantsRequest = () => ({
  type: actionTypes.REQUEST
});
export const getRestaurantsSuccess = (payload: any) => ({
  type: actionTypes.SUCCESS,
  payload
});
export const getRestaurantsError = (payload: any) => ({
  type: actionTypes.ERROR,
  payload
});

export const getRestaurants = () => {
  return (dispatch: any) => {
    dispatch(getRestaurantsRequest());

    return getRestautrantsAPI()
      .then((res) => {
        dispatch(getRestaurantsSuccess(res));
      })
      .catch((err) => {
        dispatch(getRestaurantsError(err));
        throw err;
      });
  }
};
