import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';

const reducers = {
  restaurants: require('./restaurants.reducer.ts')
};

export default createStore(combineReducers(reducers), compose(applyMiddleware(thunkMiddleware)));
