import { actionTypes } from '../actions/restaurants.actions';

const defaultState: any = {
  loading: false,
  loaded: false,
  error: false,
  restaurants: []
};

module.exports = (state=defaultState, action: any) => {
  switch (action.type) {
    case actionTypes.REQUEST:
      return {
        ...state,
        loading: true
      };

    case actionTypes.SUCCESS:
      const { restaurants } = action.payload;

      return {
        ...state,
        loading: false,
        loaded: true,
        error: false,
        restaurants
      };

    case actionTypes.ERROR:
      return {
        ...state,
        loading: false,
        error: true
      };

    default:
      return state;
  }
};
