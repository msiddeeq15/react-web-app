import * as React from 'react';
import { connect } from 'react-redux';
import { getRestaurants } from '../actions/restaurants.actions';

function withRestaurants(Component: any): any { 
  const mapStateToProps = (state: any) => {
    const { restaurants, loading, error } = state.restaurants;
  
    return {
      loading,
      error,
      restaurants
    };
  };
  
  const mapDispatchToProps = {
    getRestaurants
  };

  return connect(mapStateToProps, mapDispatchToProps)(
    class extends React.Component<any> {
      componentDidMount() {
        const { getRestaurants } = this.props;

        getRestaurants();
      }

      render() {
        const { loading, error, restaurants } = this.props;
        const props = {
          loading,
          error,
          restaurants
        };

        return <Component { ...props } />;
      }
    }
  );
}

export default withRestaurants;

