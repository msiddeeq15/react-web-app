import * as React from "react";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
  InfoWindow
} from "react-google-maps";
import { getMidPoint } from '@Utils';
import './LunchMap.less';

export interface MarkerProps {
  point: {
    lat: number;
    lng: number;
  };
  name?: string;
  category?: string;
  phone?: string;
  onClick?: () => void
}

export interface LunchMapProps {
  markers?: Array<MarkerProps>;
  handleMarkerSelect?: () => void;
}

const MarkerInfo = (props: MarkerProps) => (
  // onClick not firing when clicked
  <div onClick={ props.onClick } className="MarkerInfo">
    <a><h3>{ props.name }</h3></a>
    <div>{ props.category }</div>
    <div>{ props.phone }</div>
  </div>
)

const MapWithMarkers = withScriptjs(withGoogleMap(class extends React.Component<any> {
  state: Readonly<any> = {
      markerIndex: null
  };

  setActiveMarker(index?: number) {
    if(typeof index === 'number') {
      if(index === this.state.markerIndex) {
        this.props.handleMarkerSelect(index);
      } else {
        this.setState({ markerIndex: index });
      }
    } else {
      this.setState({ markerIndex: null });
    }
  }

  render() {
    const centerPoint = getMidPoint(this.props.markers.map((marker: MarkerProps) => ([ marker.point.lat, marker.point.lng ])));
  
    return (
      <GoogleMap
        zoom={ this.props.markers.length > 1 ? 13 : 16 }
        center={ { lat: centerPoint[0], lng: centerPoint[1] } }
        onClick={ () => this.setActiveMarker() }
      >
        {
          this.props.markers.map((marker: MarkerProps, index: number) => (
            <Marker
              key={ `marker-${ index }` }
              position={ marker.point }
              onClick={ () => this.setActiveMarker(index) }
            >
              { this.state.markerIndex === index && <InfoWindow onCloseClick={ () => this.setActiveMarker() }><MarkerInfo { ...marker } onClick={ () => this.props.handleMarkerSelect(index) }>HEY</MarkerInfo></InfoWindow>  }
            </Marker>
          ))
        }
      </GoogleMap>
    );
  }
}));



const LunchMap = (props: LunchMapProps) => (
      <div className="LunchMap">
        <MapWithMarkers
          googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyBoOFHRTTOEbsHdAxK98saCXBikYW-qMtc&v=3.exp&libraries=geometry,drawing,places"
          loadingElement={<div style={{ height: `100%` }} />}
          containerElement={<div className="MapContainer" />}
          mapElement={<div style={{ height: `100%` }} />}
          markers={ props.markers }
          handleMarkerSelect={ props.handleMarkerSelect }
        />
      </div>
    );

export default LunchMap;