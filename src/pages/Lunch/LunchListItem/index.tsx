import * as React from "react";
import classnames from 'classnames';
import './LunchListItem.less';

export interface LunchListItemProps {
  index: number,
  className?: string,
  backgroundImageURL: string,
  name: string,
  category: string,
  active?: boolean,
  onClick: (e: Object, listItem: Object) => void
}

const LunchListItem = (props: LunchListItemProps) => {
  const { index, name, category } = props;

  return (
    <div
      id={ `lunch-item-${ index }` }
      className={ classnames('LunchListItem', props.className, { active: props.active }) }
      onClick={ (e) => props.onClick(e, { index, name, category }) }
    >
      <img className="LunchListItem-image" src={ props.backgroundImageURL } />
      <div className="LunchListItem-description">
        <h3 className="heading">{ props.name }</h3>
        <div className="info">{ props.category }</div>
      </div>
    </div>
  );
}

export default LunchListItem;