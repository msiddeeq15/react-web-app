import * as React from 'react';
import classnames from 'classnames';
import { Page, Icon, List } from '@Components';
import LunchListItem from './LunchListItem';
import LunchDetail, { LunchDetailProps } from './LunchDetail';
import LunchMap from './LunchMap';
import './LunchPage.less';

export interface LunchProps {
  className?: string;
  loading?: boolean;
  error?: boolean;
  restaurants: Array<LunchDetailProps>;
}

class Lunch extends React.Component<LunchProps> {
  state: Readonly<any> = {
      mapMode: false
  };

  constructor(props: LunchProps) {
    super(props);

    this.setActiveItem = this.setActiveItem.bind(this);
  }

  componentDidUpdate(prevProps: LunchProps) {
    if(prevProps.restaurants.length === 0 && this.props.restaurants.length > 0) {
      if(this.getSelectedLunch()) {
        setTimeout(() => this.scrollItemView(this.getSelectedId()),500);        
      } 
    }
  }

  toggleMapMode() {
    this.setState({ mapMode: !this.state.mapMode });
  }
  
  getSelectedId(): number {
    const { hash } = window.location;

    return parseInt(hash.replace(/\D/g,''), 10);
  }

  getSelectedLunch(): LunchDetailProps {
    return this.props.restaurants[this.getSelectedId()];
  }

  setActiveItem(index?: number) {
    if(typeof index === 'number') {
      this.scrollItemView(index)
      window.location.hash = `#lunchItem${ index }`;
    } else {
      window.location.hash = '';
    }
  }

  getRestaurants(): Array<LunchDetailProps> {
    return this.props.restaurants.map((r: LunchDetailProps, i) => ({ ...r, active: i === this.getSelectedId() }));
  }

  scrollItemView(index: number) {
    const itemNode = document.querySelector(`#lunch-item-${ index }`);

    setTimeout(() => {
      const itemRect: any = itemNode.getBoundingClientRect();
      const topDiff = itemRect.y - 80;
      const bottomDiff = itemRect.y + itemRect.height + 50 - innerHeight;
      let scrollTop = 0;

      if(bottomDiff > 0) { /* Bottom off screen */
        scrollTop += bottomDiff;
      }

      if(topDiff - scrollTop < 0) { /* Top off screen */
        scrollTop += topDiff - scrollTop;
      }

      window.scrollBy(0, scrollTop);
    }, 100);
  }

  render() {
    const { props } = this;
    const selectedLunch = this.getSelectedLunch();
    const restaurants = this.getRestaurants();
    const markers = selectedLunch ? (
      [{ 
        point: { lat: selectedLunch.location.lat, lng: selectedLunch.location.lng },
        name: selectedLunch.name,
        category: selectedLunch.category,
        phone: selectedLunch.contact && selectedLunch.contact.formattedPhone
      }]
    ) : (
      props.restaurants
        .map(({ location: { lat, lng }, name, category, contact }, index) => 
          ({ point: { lat, lng }, name, category, phone: contact && contact.formattedPhone })
        )
    );
    const sidebarContent = (
      <div className="sidebarContent">
        <LunchMap markers={ markers } handleMarkerSelect={ this.setActiveItem }/>
        {
          selectedLunch ? (
            <LunchDetail { ...{ ...selectedLunch, handleClose: this.setActiveItem } }/>
          ) : ''
        }
      </div>
    );
  
    return (
      <Page 
        page="Lunch"
        title="Lunch Tyme"
        pageLoading={ props.loading }
        pageError={ props.error }
        leftContent={ selectedLunch ? <Icon kind="back" onClick={ this.setActiveItem }/> : undefined }
        rightContent={ <Icon className={ classnames('map-button', { active: this.state.mapMode }) } kind="map" onClick={ () => this.toggleMapMode() }/> }
        sidebarActive={ Boolean(selectedLunch) || this.state.mapMode }
        handleCloseSidebar={ this.setActiveItem }
        sidebar={ sidebarContent }
      >
        <List
          className={ classnames('LunchList', { lunchSelected: Boolean(selectedLunch) }) }
          listData={ restaurants }
          listItemComponent={ LunchListItem }
          onItemClick={ (e, listItem) => {
            this.setActiveItem(listItem.index)
          } }
        />
      </Page>
    );
  }
}

export default Lunch;