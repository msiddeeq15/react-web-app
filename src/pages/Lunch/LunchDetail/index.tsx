import * as React from "react";
import { Icon } from '@Components';
import './LunchDetail.less';

export interface LunchDetailProps {
  backgroundImageURL?: string,
  name?: string,
  category?: string,
  contact?: {
    phone?: string,
    formattedPhone?: string,
    twitter?: string,
    facebook?: string,
    facebookUsername?: string,
    facebookName?: string
  },
  location?: {
    address?: string,
    crossStreet?: string,
    lat: number,
    lng: number,
    postalCode?: string,
    cc?: string,
    city?: string,
    state?: string,
    country?: string,
    formattedAddress?: Array<string>,
  },
  handleClose?: () => void
}

const LunchDetail = (props: LunchDetailProps) => (
  <div className="LunchDetail">
    {
      props.handleClose ? <Icon className="close-button" kind="close" onClick={ () => props.handleClose() } /> : ''
    }
    <div className="LunchDetail-heading">
      <h3 className="heading">{ props.name }</h3>
      <div className="info">{ props.category }</div>
    </div>
    <div className="LunchDetail-content">
      <div className="LunchDetail-image-container">
        <img src={ props.backgroundImageURL } />
      </div>
      <div className="LunchDetail-info">
        {
          props.location ? (
            <div className="LunchDetail-address">
              <div className="info">{ props.location.address }</div><br/>
              <div className="info">{ props.location.city },</div>
              <div className="info">{ props.location.state }</div>
              <div className="info">{ props.location.postalCode }</div>
            </div>
          ) : ''
        }
        {
          props.contact ? (
            <div className="LunchDetail-contact">
              <div className="info">{  props.contact.formattedPhone }</div>
              <div className="info">{ props.contact.twitter ? `twitter: @${ props.contact.twitter }` : '' }</div>
              <div className="info">{ props.contact.facebookUsername ? `facebook: ${ props.contact.facebookUsername }` : '' }</div>
            </div>
          ) : ''
        }
      </div>
    </div>
  </div>
);

export default LunchDetail;