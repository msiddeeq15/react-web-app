import * as React from "react";
import { Page, Icon } from '@Components';
import './InternetPage.less';

const PageHeaderContentLeft = () => (
  <div className="browserActions">
    <Icon kind="back" onClick={ () => window.history.go(-1) }/>
    <Icon kind="refresh" onClick={ () => window.history.go(0) }/>
    <Icon kind="forward" onClick={ () => window.history.go(1) }/>
  </div>
)

const Internet = () => (
  <Page 
    page="Internet"
    leftContent={ <PageHeaderContentLeft /> }
  >
    { /* Dummy Background 'Content' */ }
  </Page>
);

export default Internet;