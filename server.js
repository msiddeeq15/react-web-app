
var express = require('express');
var bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.set('port', (process.env.PORT || 5001));
app.use(express.static(__dirname + '/build'));

app.get(['/', '/internet/'], function(req, res) {
  res.sendfile('./build/index.html');
});

app.get('/api/restaurants', function(req, res) {
  res.sendfile('./restaurant-data.json');
});

app.listen(app.get('port'), function() {
  console.log("Node app is running at localhost:" + app.get('port'));
});
