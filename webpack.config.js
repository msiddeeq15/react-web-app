const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    mode: 'development',
    devtool: "source-map",
    resolve: {
        modules: [__dirname, 'node_modules'],
        extensions: ['.js', '.jsx', ".ts", ".tsx", ".json"],
        alias: {
            '@Assets': path.resolve(__dirname, 'src/assets'),
            '@Components': path.resolve(__dirname, 'src/components'),
            '@Pages': path.resolve(__dirname, 'src/pages'),
            '@Utils': path.resolve(__dirname, 'src/utilities')
        }
    },
    entry: path.join(process.cwd(), '/src/index.tsx'),
    output: {
        publicPath: '/',
        path: path.resolve(__dirname, 'build'),
        filename: 'app.bundle.js',
    },
    plugins: [
        new HtmlWebpackPlugin({ 
            template: path.join(process.cwd(), '/src/index.html')
        })
    ],
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                use: 'babel-loader',
                exclude: /(node_modules|bower_components)/
            },
            { 
                test: /\.tsx?$/,
                loader: "awesome-typescript-loader"
            },
            {
                test: /\.(css|less)$/,
                use: [
                    { loader: "style-loader" },
                    { loader: "css-loader" },
                    { loader: "less-loader" }
                ]
            },
            { 
                test: /\.(jpe?g|png|gif|svg|ttf|eot|otf)$/,
                loader: 'file-loader?name=[name].[hash].[ext]', 
                exclude: /(node_modules|bower_components)/ 
            },
            { 
                test: /\.js$/,
                loader: "source-map-loader",
                enforce: "pre"
            }
        ]
    }
};
